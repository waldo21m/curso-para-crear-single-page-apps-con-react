# Curso para desarrollar servicios web REST con Nodejs

## Lecciones:

* [Introducción](#module01)
    * [Lección #1 - Requisitos previos](#lesson01)
    * [Lección #2 - Acerca del proyecto del curso](#lesson02)
    * [Lección #3 - Prepara tu entorno de trabajo](#lesson03)
* [Fundamentos](#module02)
    * [Lección #4 - Componentes página vs Componentes Estándar](#lesson04)
    * [Lección #5 - React Router](#lesson05)
    * [Lección #6 - Instalar y configurar React Router](#lesson06)
    * [Lección #7 - Rutas y Componentes](#lesson07)
* [Navegando entre páginas](#module03)
    * [Lección #8 - AppBar Material Design](#lesson08)
    * [Lección #9 - Controles formulario Login](#lesson09)
    * [Lección #10 - Componentes funcionales](#lesson10)
    * [Lección #11 - Controles formulario crear cuenta](#lesson11)
    * [Lección #12 - Rutas en componentes](#lesson12)
    * [Lección #13 - Rutas Dinámicas](#lesson13)
    * [Lección #14 - Dashboard para usuarios](#lesson14)
    * [Lección #15 - Componente para mostrar negocios](#lesson15)
* [Animar transiciones](#module04)
    * [Lección #16 - Introducción al tema de animaciones](#lesson16)
    * [Lección #17 - Switch del Router](#lesson17)
    * [Lección #18 - Definir animación en CSS](#lesson18)
    * [Lección #19 - Componentes de la animación con el router](#lesson19)
* [Consumir servicio web](#module05)
    * [Lección #20 - Configurar el servicio web](#lesson20)    
    * [Lección #21 - Colocar datos en el servicio web](#lesson21)    
    * [Lección #22 - Fetch API](#lesson22)    
    * [Lección #23 - Descargar negocios del servicio web](#lesson23)    
    * [Lección #24 - Wildcards en las rutas](#lesson24)    
    * [Lección #25 - Solicitar información individual de un negocio](#lesson25)    
    * [Lección #26 - Destructuring Assignment ES6](#lesson26)    
    * [Lección #27 - Link hacia el home en AppBar](#lesson27)    
    * [Lección #28 - Iniciar sesión y crear cuenta](#lesson28)    

## <a name="module01"></a> Introducción

### <a name="lesson01"></a> Lección #1 - Requisitos previos
- - -
Este es un curso de nivel básico de React que asume que ya sabes los fundamentos básicos de la misma librería. Para tomarlo, es necesario conocer como declarar componentes, saber que son props y states, usar de forma básica JSX y como interactuar entre componentes. Para ello se vió previamente el curso de introducción y fundamentos de React.

Además, se asume que se tiene conocimientos solidos en JavaScript, todos lostemas que estarán en este curso fueron cubiertos en el Curso profesional de JS de CodigoFacilito. Además, usaremos HTML y CSS (de forma básica e intermedia pero no es necesario saber o tener un conocimiento muy sólido, para ello esta dicho curso).

### <a name="lesson02"></a> Lección #2 - Acerca del proyecto del curso
- - -
Bienvenido al bloque de cursos para desarrollar un proyecto completo con Nodejs en el back-end y React en el front-end.

A lo largo de estos cursos, vas a adquirir los conocimientos de un desarrollador full-stack de JavaScript, lo que significa que podrás desarrollar aplicaciones web en el back-end y el front-end, con uno de los lenguajes más populares y demandados que existe.

El proyecto que vamos a crear se llama places y es una single page application (SPA), es decir, una página que se carga una sola vez y a partir de ahí, únicamente actualiza los componentes del sitio de manera dinámica usando JS y AJAX. Las SPAs como también se conocen a las singles pages applications, tienen un montón de ventajas. En general, mejoran bastante la experiencia del usuario al no tener que estar recargando el sitio web mientras recorre tu aplicación. Las SPAs son más rápidas, más responsivas, utilizan animaciones, cargan únicamente los datos que necesita en cada petición y mucho más. 

Usaremos una colección de tecnologías modernas con un mercado laboral, trabajo muy grande y promedios de sueldos bastante altos. React es una de estas tecnologías y es la librería de JS más popular que existe. Es además, uno de los proyectos Open-Source favoritos de la industria. React de hecho se usa en decena de sitios importantes como PayPal, Netflix, IG, WS Web y FB que es precisamente donde fue creada esta librería.

React revolucionó el desarrollo de aplicaciones web con su enfoque en componentes y su alto rendimiento basado en el DOM virtual que maneja, mismo que le permite únicamente actualizar las partes necesarias del DOM mismo, haciendo que tu aplicación fluya muchísimo más rápido.

Además de React, vamos a usar NodeJs con Express para el servicio web en el back-end, mongoDB para la BDD, Webpack para la configuración del proyecto en el front-end, Google Maps para los mapas y otras tecnologías. Una de estas y una de las partes más interesantes de esta colección de cursos es que cubriremos la integración de React con Redux.

Redux es un contenedor para almacenar el estatus de tu aplicación. En palabras más simples, te permite almacenar información y compartirla a lo largo de toda tu aplicación sin importar en que entorno te encuentres (que puede ser el back-end o el front-end). Esta tecnología se ha convertido en una de las favoritas de Uriel Hernández, es muy fácil almacenar o modificar la información, en bastante consistente y es muy fácil de procesar, ya que los cambios son directos gracias a que el contenedor es inmutable y las modificaciones suceden únicamente en unas funciones a las que llamamos RedUSERS.

No te preocupes si por ahora no queda muy claro alguno de estos conceptos, vamos a cubrirlos a lo largo del curso y eventualmente estarás igual de emocionado que Uriel Hernández.

El desarrollo del proyecto está dividido en múltiples cursos. Lo que permite que adelantes aquello que ya sabes y te enfoques en aquello que más necesites aprender justo ahora, que puede ser: desarrollo de servicios webs con NodeJs, fundamentos de React, React avanzado, Redux, consumir servicios webs desde JS o lo que más
necesites.

Iniciemos ya a desarrollar nuestro proyecto, te espero en el primer curso.

### <a name="lesson03"></a> Lección #3 - Prepara tu entorno de trabajo
- - -
Se necesita tener instalado:
* nodejs.
* npm.
* create-react-app.
* VSC, Atom, PHPStorm u otro IDE o editor de texto.
* Git.

Todo esto lo realizamos en las lecciones #4 y #8 del curso de introducción y fundamentos de React.

El curso irá haciendo mención de los avances que llevamos y tendremos acceso a los archivos necesario en el siguiente repositorio: https://gitlab.com/codigofacilito/introduccion-react

Por los momentos, usaremos los archivos de nuestro curso anterior en un nuevo repositorio.

## <a name="module02"></a> Fundamentos

### <a name="lesson04"></a> Lección #4 - Componentes página vs Componentes Estándar
- - -
Durante este curso, vamos a distinguir dos tipos de componentes para nuestra app, los que representan páginas completas y los que conforman las páginas.

Esta distinción la hago desde el incio del curso, porque influirá en la organización de nuestros componentes, y porque considero que es una estrategia útil para organizar el código de nuestra aplicación.

Cabe recordar que cuando construimos una Single Page App, uno de los desafíos es la organización de nuestro código, ya que tener una app completa en el cliente, implica que hay más código que escribir y que organizar.

Esta divisón, además, será importante cuando cubramos el tema de Redux en otro curso, así que por ahora mantendremos esta distinción.

Los componentes página, servirán para sustituir las vistas cuando cambiemos de ruta dentro de nuestra app, estos componentes son contenedores de otros, de aquellos que por ahora definiré como componentes estándar, mismos que encapsulan parte de la funcionalidad de la app, como por ejemplo, nuestro componente para mostrar lugares en forma de tarjeta.

Una vez que definimos esta distinción, continuemos con el curso.

### <a name="lesson05"></a> Lección #5 - React Router
- - -
React Router es una colección de componentes para navegación, que puedes integrar en tus apps de React.

Con esta colección de componentes puedes modificar la URL de tu página web, reemplazar componentes dependiendo de la dirección en la que se encuentre el usuario y mucho más.

La principal característica y diferencia del Router de React, en comparación con otras rutas, es que usa un enrutamiento dinámico, esta característica contraste con la mayoría de routers que existen, en los que la declaración de las rutas de la app, es estática.

Ahora quizás te preguntes, ¿cuál es la diferencia entre enrutamiento estático y enrutamiento dinámico? La respuesta es muy sencilla, las rutas estáticas se declaran al iniciar la app, cuando se ejecuta por primera vez la aplicación, a partir de ahí, las rutas no pueden cambiarse en tiempo de ejecución.

Por otro lado, las rutas dinámicas pueden, efectivamente, cambiar durante la ejecución del programa, como su nombre lo indica, de manera dinámica.

Otra diferencia que se distingue de esta manera de trabajar, es que las rutas no se definen en un archivo en específico, a diferencia de las rutas dinámicas donde generalmente existe un archivo para la definición de las rutas de nuestra app. Antes de ver cómo es que podemos definir rutas en cualquiera de nuestros componentes, primero veamos otra de las características del router.

El router de React está diseñado para funcionar en distintos entornos de ejecución, puede ser para el cliente, para apps nativas con React Native, o para el servidor cuando hacemos render de nuestra app desde que la enviamos al usuarios… esta es una de las características más interesantes del router, que al igual que React, puedes usar esta librería para crear apps para un montón de entornos de ejecución.

Una vez mencionados los conceptos detrás del funcionamiento del router, vamos a pasar a la configuración del mismo, para comenzar a definir las rutas de nuestra app, continuemos.

### <a name="lesson06"></a> Lección #6 - Instalar y configurar React Router
- - -
Lo primero que debemos hacer es instalar la librería react-router-dom:

```sh
$ npm install react-router-dom --save
```

También existe un react router para el uso de aplicaciones nativas... Pero, el que tiene el sufijo -dom es para el uso de aplicaciones web. Además de las pautas de esta lección podemos consultar más información en el siguiente [link](https://reacttraining.com/react-router/web/guides/quick-start).

En esta lección, vamos a implementar dos rutas usando React Router con el uso de BrowserRouter, Route y Link para que no haga peticiones extras. De esta forma vamos logrando el efecto SPA.

### <a name="lesson07"></a> Lección #7 - Rutas y Componentes
- - -
En esta lección, cambiaremos el punto de entrada de nuestra aplicación de App.js a Router.js. Podemos ver ciertas cosas:
* El hijo único de Router.js es App.js.
* Dentro de App debe haber alguno de los dos elementos (Home o Login) dependiendo de donde nos ubiquemos. Por ejemplo, si estamos en Home, el código equivalente sería algo como:
```jsx harmony
<App>
  <Home/>
</App>
```
* Hicimos uso del arreglo children *«{this.props.children}»* que se encarga de colocar los hijos en el componente App. Es decir, el ejemplo de arriba equivale a esto:
```jsx harmony
<MuiThemeProvider>
  <Home/>
</MuiThemeProvider>
```
Ahora es desde Router donde iremos insertando todos las rutas con los componentes que deseamos.

## <a name="module03"></a> Navegando entre páginas

### <a name="lesson08"></a> Lección #8 - AppBar Material Design
- - -
En esta lección, usaremos el componente AppBar de material-ui.

### <a name="lesson09"></a> Lección #9 - Controles formulario Login
- - -
En esta lección diseñaremos la página del Login haciendo uso de los componentes TextField y RaisedButton de material-ui.

### <a name="lesson10"></a> Lección #10 - Componentes funcionales
- - -
En esta lección, crearemos un componente Container (Contenedor) que se encargará de centrar nuestros elementos de distintos componentes. Es muy útil ya que si deseamos editar el tamaño o su funcionalidad se hace desde un solo sitio y no en cada uno de los archivos.

### <a name="lesson11"></a> Lección #11 - Controles formulario crear cuenta
- - -
En esta lección, usaremos como base la página de Login para hacer la página "Crear cuenta" con su respectiva ruta.

### <a name="lesson12"></a> Lección #12 - Rutas en componentes
- - -
En esta lección vamos a entender la filosofía del React Router. Este componente no sirve únicamente para actualizar toda la vista de nuestra aplicación o estar navegando de un sitio a otro como un rutero tradicional. Sirve también para cambiar componentes dependiendo de la ruta en la que nos encontremos.

Aquí, reutilizaremos el componente Login y servirá tanto para registrarnos como para iniciar sesión desde un único componente.

Otra lección importante es que podemos usar el componente Route de react-router-dom sin importar en el sitio donde nos encontremos y **que solo debemos tener una instancia del ReactRouter (BrowserRouter)** cuando estemos usando nuestras rutas, no es viable tener un ReactRouter dentro de otro ReactRouter.

Como ejercicio de esta lección, se movió lo que tenemos dentro de Route en componentes distintos para no ensuciar mucho la aplicación.

**Nota:** el prop **render** sirve solo para usar funciones mientras que el prop **component** sirve para las clases.

### <a name="lesson13"></a> Lección #13 - Rutas Dinámicas
- - -
En esta lección, veremos las maneras de como podemos manejar las rutas para usuarios que esten o no autenticados.

### <a name="lesson14"></a> Lección #14 - Dashboard para usuarios
- - -
En esta lección, diseñaremos la página del dashboard.

### <a name="lesson15"></a> Lección #15 - Componente para mostrar negocios
- - -
En esta lección, finalizaremos con el diseño de PlaceHorizontal.

## <a name="module04"></a> Animar transiciones

### <a name="lesson16"></a> Lección #16 - Introducción al tema de animaciones
- - -
Demostración de animaciones en React con una SPA.

### <a name="lesson17"></a> Lección #17 - Switch del Router
- - -
Para que existan transiciones animadas en React Router debemos usar el Switch que nos provee React Router, este cambio lo haremos en esta lección.

### <a name="lesson18"></a> Lección #18 - Definir animación en CSS
- - -
En esta lección, definiremos ahora las clases CSS que usaremos para la animación.

### <a name="lesson19"></a> Lección #19 - Componentes de la animación con el router
- - -
En esta lección implementaremos un link en el page Home que nos llevará al page de signup y realizaremos la implementación de TransitionGroup y CSSTransition en App.js.

**Importante:** En el curso, las animaciones lo realizan con React 15, por eso ha sido modificado las clases **left-out** en el archivo App.css para que funcione de forma correcta. Es importante destacar que faltaba la propiedad transition y es muy importante para realizar la animación.

## <a name="module05"></a> Consumir servicio web

### <a name="lesson20"></a> Lección #20 - Configurar el servicio web
- - -
En esta lección, usaremos los servicios que desarrollamos en el [curso para desarrollar servicios web con nodejs](https://gitlab.com/waldo21m/curso-para-desarrollar-servicios-web-rest-con-nodejs), mismo que aprendimos en https://codigofacilito.com/cursos/nodejs-servicios-web.

Lo clonaremos o lo descargamos, instalaremos las dependencias y cambiaremos el puerto al 4000 para que no colisione con el proyecto que esamos desarrollando aquí en React.

Además de esto, en el archivo **app.js** del repositorio de servicios con nodejs, debemos comentar la siguiente línea:

```js
// app.use(authApp.unless({ method: "OPTIONS" }));
```

### <a name="lesson21"></a> Lección #21 - Colocar datos en el servicio web
- - -
En esta lección, insertaremos datos a la base de datos mediante Postman. Para ello, podemos importar las colecciones que están en el repositorio y realizamos lo siguiente:
1. Creamos un usuario con el endpoint **localhost:3000/users**.
2. Creamos un lugar usando la jwt obtenida del endpoint anterior y usando el endpoint **localhost:3000/places**.
3. Es necesario tener una cuenta de cloudinary y configurarlo (en el repositorio están todos los pasos).

### <a name="lesson22"></a> Lección #22 - Fetch API
- - -
Explicación básica del fetch API. En la siguiente lección descargaremos los lugares.

### <a name="lesson23"></a> Lección #23 - Descargar negocios del servicio web
- - -
En esta lección, vamos a finalizar la función getPlaces y la usaremos para traer los lugares y mostrarlos en nuestra aplicación.

*Además de esto, se corrigen ciertos detalles para eliminar los warnings dentro del proyecto.*

### <a name="lesson24"></a> Lección #24 - Wildcards en las rutas
- - -
En esta lección, vamos a usar los parámetros en las rutas de react router dom la cual también tienen como nombre de wildcards y crearemos el componente Place que tendrá la vista individual de cada lugar.

### <a name="lesson25"></a> Lección #25 - Solicitar información individual de un negocio
- - -
En esta lección, crearemos el método getPlace en *requests/place.js* para consultar los lugares de forma individual y alimentaremos el componente que desarrollamos en la lección anterior con esta consulta.

### <a name="lesson26"></a> Lección #26 - Destructuring Assignment ES6
- - -
También llamada **asignación desestructurante** y es una expresión que posibilita la extracción de datos de arrays, o de propiedades de objetos, en variables distintas. Podemos ver más información y con ejemplos en [Asignación Desestructurante](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Destructuring_assignment).

En esta lección realizaremos el siguiente refactor. Para reducir la sintáxis **this.state.place.etc** lo podemos realizar de la siguiente manera:

```js
const {place} = this.state;
```

Donde **{place}** debe coincidir con el mismo nombre que se asignó en el state para que ese valor pueda ser asignado. Si en el state existiese una propiedad user podemos asignarlo de la siguiente manera:

```js
const {place, user} = this.state;
```

Esto nos ayuda a simplificar la sintáxis de nuestro código y podemos usarlo en el resto de los componentes.

### <a name="lesson27"></a> Lección #27 - Link hacia el home en AppBar
- - -
En esta lección, implementaremos una función goHome en **App.js** que se lo pasaremos como un prop al componente **MyAppBar**, esta función lo que hará es modificar el prop history que nos provee withRouter de react router dom *(esto es un prop mutable)*, logrando así modificar el router para que vaya a la página principal de nuestra aplicación.

*Nota: Se elimina el uso de withRouter en el componente Place.js y esto ocurre porque no hace falta usarlo nuevamente en el proyecto ya que está siendo llamado en App.js*

### <a name="lesson28"></a> Lección #28 - Iniciar sesión y crear cuenta
- - -
En esta última lección del curso, vamos a crear los métodos login y signUp que servirán para iniciar sesión y crear una cuenta respectivamente. La continuación de este curso será con el [Curso para integrar Redux con React](https://codigofacilito.com/videos/introduccion-al-curso-de-redux).

*Importante: https://learnwithparam.com/blog/how-to-pass-props-in-react-router/*